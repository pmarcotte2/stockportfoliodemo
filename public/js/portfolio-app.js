define(["marionette"], function(Marionette){

	// PortfolioApp
	var PortfolioApp = new Marionette.Application();
		PortfolioApp.basePath = '';
	PortfolioApp.version = "v0.0.1";
	PortfolioApp.apiRoot =  PortfolioApp.basePath + "/api";
	
	PortfolioApp.addRegions({
		mainRegion: "#main-region",
		dialogRegion: "#dialog-region",
	});
	
	PortfolioApp.globals = new Backbone.Model({
		showLog: true,
	});
	
	
	// HANDLERS
	PortfolioApp.reqres.setHandler("globals:showLog", function(boolean) {
			 
		if (PortfolioApp.globals.get('showLog') === true && (boolean === true || boolean === false)) {
			return boolean;
		}
		return PortfolioApp.globals.get('showLog');
	});
	
		
	// INITIALIZE Application
	PortfolioApp.on("start", function(options) {
		console.log("PortfolioApp has started, with options:", options);

		PortfolioApp.options = options;
		
	});


	return PortfolioApp;
	
});

