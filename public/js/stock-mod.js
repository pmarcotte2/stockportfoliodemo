define(["portfolio-app"], function(PortfolioApp) {

	PortfolioApp.module("StockMod", function(Price, PortfolioApp, Backbone, Marionette, $, _) {
			
		var wantLog = false;
		var showLog = PortfolioApp.request("globals:showLog", wantLog);

		// Model & Collection
		var Stock = Backbone.Model.extend({
			defaults: {
				price: "",
			},
			comparator: "symbol",
		});
		
		var StockPortfolio = Backbone.Collection.extend({
			model: Stock,
			url: function() {
				return PortfolioApp.apiRoot + '/stock';
			},
			//Parse HAL response from server
			// See: https://apigility.org/documentation/api-primer/halprimer#collections
			parse: function (response) {
				if (response) {
					this.set(response._embedded.stock);
					
					if (response.page_count != undefined) {
						this.pagination = {
							pageCount: response.page_count,
							pageSize: response.page_size,
							totalItems: response.total_items
						};
						
						this.links = response._links;
					}
				} else {
					this.set();
				}
				return this.models;
			},
		});
		
		
		// Views
		var StockItemView = Marionette.ItemView.extend({
			model: Stock,
			template: "#stock-item-template",
			tagName: "tr",
			ui: {
				price: ".price",
				delete: ".js-delete",
			},
			events: {
				"click @ui.delete": "delete",
			},
	  		delete: function(e) {
				e.stopPropagation();
	
				result = window.confirm("Deleting cannot be undone. Delete this item?");
	
				if(result === true) {
					this.model.destroy();
				}
			},
			onRender: function() {
				var theView = this;
				var gettingPrice = $.ajax("https://query.yahooapis.com/v1/public/yql?format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback&q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%3D%22" + this.model.get('symbol') + "%22")
				$.when(gettingPrice).done(function(data) {
					var price = data.query.results.quote.LastTradePriceOnly;
					if (price === null) {
						price = "None found."
					}
					theView.ui.price.html(price);
				});
			},
		});
		
		var StockPortfolioView = Marionette.CompositeView.extend({
			childView: StockItemView,
			childViewContainer: "tbody",
			emptyView: NoItemView,
			template: "#stock-portfolio-template",
			ui: {
				add: "#js-add",
				symbol: "#js-symbol",
			},
			events: {
				"click @ui.add": "add",
			},
			add: function(e) {
				e.stopPropagation();
				var symbol = this.ui.symbol.val();
				symbol = symbol.toUpperCase();
				console.log(symbol);
				this.collection.create({symbol: symbol});
			}
		});

		var NoItemView = Marionette.ItemView.extend({ tagName: "dt", template: "#no-item-template", });


		// INITIALIZE Module
		this.addInitializer(function(options) {
			console.log("PortfolioApp.Price module started.");
			
			stockPortfolio = new StockPortfolio();
			stockPortfolio.fetch();
			
			PortfolioApp.mainRegion.show(new StockPortfolioView({collection: stockPortfolio}));
    
    	});
	});
	
	//return ;
	return PortfolioApp;
	
});
	
