/*---------------------------------
	 BOOTSTRAP POPOVER EXTRAS
-----------------------------------*/	

/*---- Hides popover if window is resized ----*/
$(window).resize(function(){ 
	if ($('.popover').is(":visible") && $('.popover').has(":visible")) { 
		$('.slide-caption').popover('toggle');
	} 
	
}); 

/*---- Makes it possible to click outside popover to close ----*/

//A note about the use of $(':not(#anything)') as the body selector. This is due to iOS not binding click events to ‘html’ or ‘body’.
//To put it short, this is a bulletproof way to detect clicks anywhere on the document (providing you don’t have a div with an id of ‘anything’).
$(':not(#anything)').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons and other elements within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
            return;
        }
    });
});
