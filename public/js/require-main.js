requirejs.config({
	baseURL: "js/",
	paths: {
		backbone: "../vendor/backbone/backbone",
		bootstrap: "../vendor/bootstrap/js/bootstrap",
		jquery: "../vendor/jquery/jquery.min",
		json2: "../vendor/backbone/json2",
		marionette: "../vendor/backbone/backbone.marionette",
		underscore: "../vendor/backbone/underscore.min",
	},
	shim: {
		underscore: {
			exports: "_"
		},
		backbone: {
			deps: ["jquery", "underscore", "json2"],
			exports: "Backbone"
		},
        "bootstrap": {
          deps: ["jquery"],
          exports: "$.fn.popover"
        },
		marionette: {
			deps: ["backbone"],
			exports: "Marionette"
		},
		//"jquery-ui": ["jquery"]
	}
});

require(["portfolio-app", "stock-mod"], function(PortfolioApp) {
	console.log("jQuery version: ", $.fn.jquery);
	console.log("underscore identity call: ", _.identity(5));
	console.log("Marionette: ", Marionette.VERSION);
	PortfolioApp.start({
		appDiv: "#main-region",
	});
});
