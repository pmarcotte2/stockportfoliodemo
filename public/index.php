<?php
	error_reporting(-1);
	ini_set('display_errors',1);
// Define application environment if not already defined by Apache
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));
    
if (getenv('APPLICATION_ENV') == 'development' || getenv('APPLICATION_ENV') == 'testing' ) {
	/* DO NOT USE FOR PRODUCTION **/
	error_reporting(-1);
	ini_set('display_errors',1);
}

// Time Zone
$timezone = "America/Denver";
if(function_exists('date_default_timezone_set')) {date_default_timezone_set($timezone);} 

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
//Zend\Mvc\Application::init(require 'config/application.config.php')->run();

// Using this for Apigility instead of default ->run() line.

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}
$appConfig = include APPLICATION_PATH . '/config/application.config.php';
if (file_exists(APPLICATION_PATH . '/config/development.config.php')) {
    $appConfig = Zend\Stdlib\ArrayUtils::merge($appConfig, include APPLICATION_PATH . '/config/development.config.php');
}
// Run the application!
Zend\Mvc\Application::init($appConfig)->run();










