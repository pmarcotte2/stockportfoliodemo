DEN-Skeleton-App
=======================

Introduction
------------
This is a simple, skeleton application using the ZF2 MVC layer and module systems.  Den-skeleton-app includes libraries which faciliate responsive and mobile-first development.


LIBRARIES

Application
-----------

* jQuery v1.11.1

* Backbone v1.1.2
	- Description: Backbone.js gives structure to web applications by providing models with key-value binding and custom events, collections with a rich API of enumerable functions, views with declarative event handling, and connects it all to your existing API over a RESTful JSON interface.
	- Website: http://backbonejs.org/

	* json2.js
		- Description: json2.js: This file creates a JSON property in the global object, if there isn't already one, setting its value to an object containing a stringify method and a parse method.
		- Website: https://github.com/douglascrockford/JSON-js
	
	* Underscore.js 1.6.0
		- Description: Underscore is a JavaScript library that provides a whole mess of useful functional programming helpers without extending any built-in objects.
	 	- Website: http://underscorejs.org

* Backbone.Syphon v0.5.0
	- Description: Serialize a Backbone.View in to a JavaScript object. Used for FORM interactions.
	- Website: https://github.com/marionettejs/backbone.syphon

* Marionette v2.1.0
	- Description: Backbone.Marionette is a composite application library for Backbone.js that aims to simplify the construction of large scale JavaScript applications.
	- Website: http://marionettejs.org/


CSS
---

* Bootstrap v3.2.0
	
	- Description: Mobile-first front-end framework.
	- Website: http://getbootstrap.com/


* Font Awesome v4.1.0
	
	- Description: Icon font designed for Bootstrap.  Bootstrap comes with Glyphicons, but the selection of icons are very limited.  Font Awesome gives you scalable vector icons that can instantly be customized — size, color, drop shadow, and anything that can be done with the power of CSS. 
	- Website: http://fontawesome.io/


* Jasny Bootstrap v3.1.3
	
	- Description: Jasny Bootstrap is an extension of bootstrap and adds a number of features and components.
	- Dependencies: Bootstrap 3.0+
	- Webstite: http://jasny.github.io/bootstrap/
	

* bxSlider v4.1.1
	
	- Description: bxSlider is fully responsive and will adapt to any device.  Slides can contain images, video, or HTML content.  Advanced touch/swipe support is built-in. 
	- Website: http://bxslider.com/ 


* Magnific Popup v0.9.9
	
	- Description: Magnific Popup is a responsive lightbox & dialog script with focus on performance and providing the best experience for users on any device.
	- Dependencies: jQuery 1.8.0+
	- Website: http://dimsemenov.com/plugins/magnific-popup/


* Tablesorter v2.17.7
	
	- Description: Tablesorter is a jQuery plugin for turning a standard HTML table with THEAD and TBODY tags into a sortable table without page refreshes.
	- Dependencies: jQuery v1.2.6+
	- Website: http://mottie.github.io/tablesorter/docs/


* Select2 v3.5.1
	
	- Description: Select2 is a jQuery based replacement for select boxes. It supports searching, remote data sets, and infinite scrolling of results.
	- Dependencies: jQuery v1.7.1+
	- Website: http://ivaynberg.github.io/select2/


* Placeholders v3.0.2
	
	- Description: Placeholders.js is a JavaScript polyfill for the HTML5 placeholder attribute. It's lightweight, has zero dependencies and works in pretty much any 	browser you can imagine. It enables browsers that don't support the HTML5 placeholder attribute to show the placeholder text in input fields and textareas.  Browsers that support the placeholder attribute natively will be unaffected and work as usual. 
	- Notes: Bundled with jQuery adapter
	- Website: http://jamesallardice.github.io/Placeholders.js/


* Respond v1.4.2
	- Description: A fast & lightweight polyfill for min/max-width CSS3 Media Queries (for IE 6-8, and more). It enables responsive web designs in browsers that don't support CSS3 media queries.
	- Website: https://github.com/scottjehl/Respond



* Modernizr v2.8.3
	- Description: Modernizr is a JavaScript library that detects HTML5 and CSS3 features in the user’s browser.  Modernizr runs quickly on page load to detect features; it then creates a JavaScript object with the results, and adds classes to the html element for you to key your CSS on.  Modernizr makes it easy for you to write conditional JavaScript and CSS to handle each situation, whether a browser supports a feature or not. It’s perfect for doing progressive enhancement easily.  As of Modernizr 1.5, this script is identical to what is used in the popular html5shim/html5shiv library, so we don't need html5.js or html5shiv.js.
	- Website: http://modernizr.com/


IE8 Dependencies
------------------

- respond.min.js
- placeholders.jquery.min.js
