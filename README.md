Stock Portfolio Demo Application
=====================

Introduction
------------
This is a web application to demonstrate
the use of ZF2, Doctrine ORM, Apigility for api,
BackboneJS, MarionetteJS, and Bootstrap 3.

Dependencies
------------
- PHP dependencies (/vendor) are listed in composer.json

Installation
------------
# Once you have a copy of the repo on your local development machine,
cd to/your/project/directory and run:

php composer.phar install

to install the vendor/ dependencies.

# Create Database
Create a MySQL db from /data/database/schema.sql

See /config/autoload/database.dev.global.php for credentials.

# Setup up your localhost / virtual host

Then visit the project's index page with your web browser.

Client-Side App
---------------
The files portfolio-app.js, and stock-mod.js contain the client-side app.
They are wrapped inside requireJS.

Enjoy!
