<?php
return array(
    'router' => array(
        'routes' => array(
            'stock-api.rest.stock' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/api/stock[/:stock_id]',
                    'defaults' => array(
                        'controller' => 'StockApi\\V1\\Rest\\Stock\\Controller',
                    ),
                ),
            ),
        ),
    ),
    'zf-versioning' => array(
        'uri' => array(
            0 => 'stock-api.rest.stock',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'StockApi\\V1\\Rest\\Stock\\StockResource' => 'StockApi\\V1\\Rest\\Stock\\StockResourceFactory',
        ),
    ),
    'zf-rest' => array(
        'StockApi\\V1\\Rest\\Stock\\Controller' => array(
            'listener' => 'StockApi\\V1\\Rest\\Stock\\StockResource',
            'route_name' => 'stock-api.rest.stock',
            'route_identifier_name' => 'stock_id',
            'collection_name' => 'stock',
            'entity_http_methods' => array(
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ),
            'collection_http_methods' => array(
                0 => 'GET',
                1 => 'POST',
            ),
            'collection_query_whitelist' => array(),
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => 'StockApi\\V1\\Rest\\Stock\\StockEntity',
            'collection_class' => 'StockApi\\V1\\Rest\\Stock\\StockCollection',
            'service_name' => 'Stock',
        ),
    ),
    'zf-content-negotiation' => array(
        'controllers' => array(
            'StockApi\\V1\\Rest\\Stock\\Controller' => 'HalJson',
        ),
        'accept_whitelist' => array(
            'StockApi\\V1\\Rest\\Stock\\Controller' => array(
                0 => 'application/vnd.stock-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ),
        ),
        'content_type_whitelist' => array(
            'StockApi\\V1\\Rest\\Stock\\Controller' => array(
                0 => 'application/vnd.stock-api.v1+json',
                1 => 'application/json',
            ),
        ),
    ),
    'zf-hal' => array(
        'metadata_map' => array(
            'StockApi\\V1\\Rest\\Stock\\StockEntity' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'stock-api.rest.stock',
                'route_identifier_name' => 'stock_id',
                'hydrator' => 'Zend\\Stdlib\\Hydrator\\ArraySerializable',
            ),
            'StockApi\\V1\\Rest\\Stock\\StockCollection' => array(
                'entity_identifier_name' => 'id',
                'route_name' => 'stock-api.rest.stock',
                'route_identifier_name' => 'stock_id',
                'is_collection' => true,
            ),
        ),
    ),
);
