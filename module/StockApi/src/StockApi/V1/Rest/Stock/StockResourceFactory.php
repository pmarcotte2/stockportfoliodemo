<?php
namespace StockApi\V1\Rest\Stock;

class StockResourceFactory
{
    public function __invoke($services)
    {
        return new StockResource(
			$services->get('stock_mapper')
        );
    }
}
