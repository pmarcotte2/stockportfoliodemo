<?php
namespace StockApi\V1\Rest\Stock;

use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;

class StockResource extends AbstractResourceListener
{
    protected $mapper;

    public function __construct($mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
    	$stock = $this->mapper->create((array) $data);
    	$this->mapper->save($stock);
    	
		return $this->getRepresentation($stock);
        //return new ApiProblem(405, 'The POST method has not been defined');
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
    	$this->mapper->delete($id);
        //return new ApiProblem(405, 'The DELETE method has not been defined for individual resources');
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->getRepresentation($this->mapper->find($id));
        //return new ApiProblem(405, 'The GET method has not been defined for individual resources');
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = array())
    {
    	$stocks = $this->mapper->findAll();
    	
		foreach($stocks as $stock) {
			$stocksArray[] = $this->getRepresentation($stock);
		}
		
		return $stocksArray;
        //return new ApiProblem(405, 'The GET method has not been defined for collections');
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
    	$stock = $this->mapper->update($id, (array) $data);
    	$this->mapper->flush();
    	
        return $this->getRepresentation($stock);
        //return new ApiProblem(405, 'The PUT method has not been defined for individual resources');
    }
    
    
	public function getRepresentation($entity)
	{
		$data['id'] = $entity->getId();
		$data['symbol'] = $entity->getDisplayName();
		return $data;
	}

}
