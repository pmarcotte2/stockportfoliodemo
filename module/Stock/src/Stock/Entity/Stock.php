<?php
namespace Stock\Entity;
use Doctrine\ORM\Mapping as ORM;

/** 
 * Stock
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="stock")
 */
class Stock {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(name="stock_id", type="bigint")
     */
    protected $id;

	/** @ORM\Column(name="symbol", type="string") */
    protected $symbol;

	/** @ORM\Column(name="random_id", type="string") */
	protected $randomId;

	/** @ORM\Column(name="creator_id", type="bigint") */
	protected $creatorId;

	/** @ORM\Column(name="created_on", type="datetime") */
	protected $createdOn;

	/** @ORM\Column(name="modifier_id", type="bigint") */
	protected $modifierId;

	/** @ORM\Column(name="modified_on", type="datetime") */
	protected $modifiedOn;
	

/** GETTERS AND SETTERS */

	public function getId()
	{
		return $this->id;
	}


	public function setSymbol($str)
		{
		 $this->symbol = strtoupper($str);
	}

	public function getSymbol()
	{
		return $this->symbol;
	}

	public function getDisplayName()
	{
		return $this->getSymbol();
	}
	
/** METADATA EVENT CALLBACKS */
	
    /** @ORM\PrePersist */
    public function doDenOnPrePersist()
    {
        $this->setRandomId();
        $this->setCreatedOn();
    }

    /** @ORM\PreUpdate */
    public function doDenOnPreUpdate()
    {
        $this->setModifiedOn();
    }
    
	protected function setRandomId()
	{
		if (empty($this->randomId)) {
			$this->randomId = \Zend\Math\Rand::getString(28, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_', true);
		}
	}

	public function getRandomId()
	{
		return $this->randomId;
	}

	protected function setCreatedOn()
	{
    	$dateTime = new \DateTime('NOW');
		$this->createdOn = $dateTime;
	}

	public function getCreatedOn()
	{
		return $this->createdOn;
	}

	protected function setModifiedOn()
	{
    	$dateTime = new \DateTime('NOW');
		$this->modifiedOn = $dateTime;
	}

	public function getModifiedOn()
	{
		return $this->modifiedOn;
	}
}