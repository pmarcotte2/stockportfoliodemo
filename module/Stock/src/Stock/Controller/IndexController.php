<?php
namespace Stock\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Client;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    
	public function testCreateAction()
	{	
	    if ($this->request->isPost()) {

	        $data = $this->request->getPost();

			$mapper = $this->getServiceLocator()->get('stock_mapper');   
	        $stock = $mapper->create(array('symbol' => strtoupper($data['symbol'])));
	        $mapper->save($stock);

	    	print($stock->getDisplayName() . " (id " . $stock->getId() . " - " . $stock->getRandomId() . ") was created.");
	    
	   		exit;
	    }
	}
	
	public function testFindAllAction()
	{
		$mapper = $this->getServiceLocator()->get('stock_mapper');
		
		print_r($mapper->findAll());
		
		exit;
	}

	public function testUpdateAction()
	{
		$mapper = $this->getServiceLocator()->get('stock_mapper');
		
		print_r($mapper->update('2', array('symbol' => 'GOOG')));
		$mapper->flush();
		
		exit;
	}
}
