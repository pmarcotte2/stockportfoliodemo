<?php
namespace Stock\Mapper;

use Doctrine\Common\Persistence\ObjectManager;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Stdlib\Hydrator\HydratorInterface;

class StockMapper implements ServiceLocatorAwareInterface
{
	/**
	 * Mapper's Default entity class.
	 * Override it if your mapper inherits from DenAppMapper.
	 */
	const ENTITY_FQCN = 'Stock\Entity\Stock';
	
	/**
	 * @var Zend/Stdlib/Hydrator/HydratorInterface
	 */
	protected $hydrator;
			
	/**
	 * @var Doctrine\Common\Persistence\ObjectManager
	 */
	protected $persistence;

	/** 
	 * @var Zend/ServiceManager/ServiceManager 
	 */
	protected $serviceManager;
	

	public function __construct(ObjectManager $persistence, HydratorInterface $hydrator)
	{
		$this->persistence = $persistence;
		$this->hydrator = $hydrator;
	}
		
	public function create(Array $data = null, $fqcn = null)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		
		$item = new $fqcn();
		
		if ($data) {
			$this->hydrator->hydrate($data, $item);
		}
		
		$this->persistence->persist($item);
		
		return $item;
	}

	public function update($idOrObj, Array $data, $fqcn = null, $isRandomId = false)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		
		if (is_string($idOrObj) == true) {
			$obj = $this->find($idOrObj, $fqcn, $isRandomId);
		} else {
			$obj = $idOrObj;
		}
				
		$this->hydrator->hydrate($data, $obj);
		
		$this->persistence->persist($obj);
		
		return $obj;
	}

	public function save($obj)
	{
		$this->persistence->persist($obj);
		$this->persistence->flush();
		
		return $obj;
	}
	
	// Removes from UnitOfWork -- not persistence.
	public function remove($obj)
	{
		$this->persistence->remove($obj);
	}

	/**
	 * Deletes from persistence
	 *
	 * @param mixed id|object
	 * 
	 * To use DQL: DELETE MyProject\Model\User u WHERE u.id = 4
	 */
	public function delete($idOrObj, $fqcn = null, $isRandomId = false)
	{
		if (is_string($idOrObj) == true) {
			$obj = $this->find($idOrObj, $fqcn, $isRandomId);
		} else {
			$obj = $idOrObj;
		}
	
		$this->persistence->remove($obj);
		$this->persistence->flush();
	}

	/**
	 * Find by id
	 */
	public function find($id, $fqcn = null, $isRandomId = false)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		if ($isRandomId == true) {
			$obj = $this->findOneBy(array(
				'randomId' => $id
			), null, null, null, $fqcn); // returns Array()
		} else {
			$obj = $this->persistence->getRepository($fqcn)->find($id);
		}
		
		return $obj;
	}
	
	public function findBy($criteria, $orderBy = null, $limit = null, $offset = null, $fqcn = null)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		
		return $this->persistence->getRepository($fqcn)->findBy($criteria, $orderBy, $limit, $offset);
	}
	
	public function findOneBy($criteria, $orderBy = null, $limit = null, $offset = null, $fqcn = null)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		
		return $this->persistence->getRepository($fqcn)->findOneBy($criteria, $orderBy, $limit, $offset);
	}
	
	public function findAll($fqcn = null)
	{
		if ($fqcn === null) {
			$fqcn = $this->getDefaultFqcn();
		}
		
		return $this->persistence->getRepository($fqcn)->findAll();
	}

	public function persist($obj)
	{
		return $this->persistence->persist($obj);
	}
	
	public function flush()
	{
		$this->persistence->flush();
	}
	
	
	/**	UTILITY */

	public function getDefaultFqcn()
	{
		if (static::ENTITY_FQCN == null) {
			throw new \Exception('Mapper FQCN is not defined.');
		}
		return static::ENTITY_FQCN;
	}
	
	public function getObjectManager()
	{
		return $this->persistence;
	}
	
	
	/**  SERVICE LOCATOR AWARE INTERFACE */

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
    	$this->serviceManager = $serviceLocator;
    }

    /**
     * Get service locator
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
   {
   		return $this->serviceManager;
   }
}
