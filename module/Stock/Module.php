<?php
namespace Stock;

use Stock\Mapper\StockMapper;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject as DoctrineHydrator;

use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\View\Renderer\PhpRenderer;
use Zend\View\Resolver\TemplateMapResolver;

class Module
{
	public function getAutoloaderConfig()
	{
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );	
	}

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
    
    public function getServiceConfig()
    {
    	return array(
    		'invokables' => array(
    		),
	    	'factories' => array(
				'stock_mapper' => function($sm) {
					$objMgr = $sm->get('app_object_manager');
	                $hydrator = $sm->get('app_default_hydrator');
	                return new StockMapper($objMgr, $hydrator);
				},
	    	),
	    );
    }

    public function getViewHelperConfig()
    {
        return array(
    		'invokables' => array(
    		),
    		'factories' => array(
    		),
        );
	}
}


