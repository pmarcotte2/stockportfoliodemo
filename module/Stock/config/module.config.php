<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Stock\Controller\Index' => 'Stock\Controller\IndexController',
        ),
    ),
	'doctrine' => array(
	  'driver' => array(
	    'stock_entities' => array(
	      'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
	      'cache' => 'array',
	      'paths' => array(__DIR__ . '/../src/Stock/Entity')
	    ),
	    'orm_default' => array(
	      'drivers' => array(
	      	'Stock\Entity' => 'stock_entities',
	      )
		)
	)),
    'router' => array(
        'routes' => array(
            'stock' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/stock[/][:action][/][:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[A-Za-z0-9_-]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Stock\Controller\Index',
                        'action'     => 'index',
                	),
                ),
            ),
        ),
    ),
    'view_manager' => array(
/*
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
*/
        'template_path_stack' => array(
            'ata-title' => __DIR__ . '/../view',
        ),
        //'layout' => 'layout/layout',
    ),
);