<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Zend\Http\Client;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
		return $this->redirect()->toRoute('stock');
    }
    
}
