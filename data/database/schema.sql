CREATE TABLE `stock` (
  `stock_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `symbol` varchar(255) DEFAULT NULL,
  `random_id` char(28) NOT NULL,
  `creator_id` bigint(20) unsigned DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `modifier_id` bigint(20) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;